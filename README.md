# foliate

Simple eBook viewer. https://johnfactotum.github.io/foliate/

# Issues in 1.5.3 (2019-10)
* Where are the annotations stored?
* Characters police size control works more or less, when the document contains
  more thant one font size.

# Unofficial documentation
* [*Foliate is an Epic eBook Reader for Linux Desktops (Updated)*
  ](https://www.omgubuntu.co.uk/2019/05/foliate-ebook-reader-linux)
  2019-07 Joey Sneddon

# Installing Foliate
* [repology](https://repology.org/project/foliate/versions)
* [pkgsrc](https://pkgsrc.se/print/foliate)

# Annotating ePUB
* [*A Partnership to Bring Open Annotation to eBooks*
  ](https://web.hypothes.is/blog/ebook-partnership/)
  2017-03 dwhly

## Other file format supporting annotation
* [*Comparison of e-book formats*
  ](https://en.wikipedia.org/wiki/Comparison_of_e-book_formats) (Wikipedia)